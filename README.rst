Geode-lib
=========
A little development library for the Geode projects.

The Geode-lib library is a free software redistribute under the term of the
GNU General Public License version 3.0 or later.

- https://www.gnu.org/licenses/gpl-3.0.html

Dependencies
------------
- Python >= 3.11

Authors
-------
- PacMiam: https://pacmiam.tuxfamily.org/

Retrieve source code
--------------------
To retrieve source code, you just need to use git and run the following
command::

    git clone https://framagit.org/geode/geode-lib.git

To install this project in a virtualenv::

    pip install .

Development
-----------
The development libraries can be installed in the virtualenv::

    pip install .[dev]

Tests
~~~~~
The tests can be launched with ``nox``::

    nox -s tests

Lint
~~~~
To check if ``flake8`` and ``black`` are happy::

    nox -t check

To check and apply the modification from ``black``::

    nox -t style
