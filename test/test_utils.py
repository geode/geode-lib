# ------------------------------------------------------------------------------
#  Copyleft 2021  PacMiam
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Python
from datetime import datetime, timedelta
from os import environ
from pathlib import Path

# Geode
from geode_lib.utils import (
    are_equivalent_timestamps,
    generate_extension,
    generate_identifier,
    get_boot_datetime_as_timestamp,
    get_binary_path,
    get_creation_datetime,
    glob_escape,
    parse_string_as_timedelta,
    parse_timedelta_as_string,
)
from test.utils import TestCaseMixin


class UtilsTC(TestCaseMixin):
    def test_check_different_timestamps(self):
        self.assertEqual(
            are_equivalent_timestamps(1587400601, 1587400604, delta=0), False
        )

    def test_check_different_timestamps_with_delta(self):
        self.assertEqual(
            are_equivalent_timestamps(1587400601, 1587400604, delta=2), False
        )

    def test_check_equivalent_timestamps(self):
        self.assertEqual(
            are_equivalent_timestamps(1587400601, 1587400601, delta=0), True
        )

    def test_check_equivalent_timestamps_with_delta(self):
        self.assertEqual(
            are_equivalent_timestamps(1587400601, 1587400604, delta=3), True
        )

    def test_generate_extension(self):
        self.assertEqual(generate_extension("NES"), "[nN][eE][sS]")

        self.assertEqual(generate_extension(".ogg"), ".[oO][gG][gG]")

        self.assertEqual(
            generate_extension(".tAr.Xz"), ".[tT][aA][rR].[xX][zZ]"
        )

    def test_generate_identifier_from_new_file(self):
        path = self.path.joinpath("new and Amazing file [!] (EU).gnu")
        path.touch()
        self.assertEqual(
            generate_identifier(path),
            f"new-and-amazing-file-eu-gnu-{path.stat().st_ino}",
        )
        self.assertEqual(
            generate_identifier(path, use_inode=False),
            "new-and-amazing-file-eu-gnu",
        )

        path.unlink()
        self.assertEqual(
            generate_identifier(path), "new-and-amazing-file-eu-gnu"
        )

    def test_generate_identifier_from_string(self):
        self.assertEqual(
            generate_identifier("From a Basic String"), "from-a-basic-string"
        )

    def test_generate_identifier_from_string_with_special_characters(self):
        self.assertEqual(
            generate_identifier("With (some)_speci[al]-chars!"),
            "with-some-speci-al-chars",
        )

    def test_generate_identifier_from_string_with_unicode_characters(self):
        self.assertEqual(
            generate_identifier("This_ís 🦊a-strange file 😀"),
            "this-ís-a-strange-file",
        )

    def test_generate_identifier_from_unknown_file(self):
        path = self.path.joinpath("unknown_file.txt")
        self.assertEqual(generate_identifier(path), "unknown-file-txt")

    def test_get_binary_path_from_custom_path(self):
        directory = self.path.joinpath("bin")
        directory.mkdir()

        filepath = directory.joinpath("binary_test")
        filepath.touch()

        self.assertEqual(len(get_binary_path("binary_test")), 0)
        self.assertGreaterEqual(len(get_binary_path(filepath)), 1)

        environ["PATH"] = f"{directory}:{environ['PATH']}"
        self.assertEqual(len(get_binary_path("binary_test")), 1)

        # Ensure to have an unreadable directory for current test
        directory.chmod(0o033)
        self.assertEqual(len(get_binary_path("binary_test")), 0)

        # Revert permissions to delete directory
        directory.chmod(0o755)

    def test_get_binary_path_from_system(self):
        self.assertGreaterEqual(len(get_binary_path("python3")), 1)
        self.assertEqual(len(get_binary_path("were-binary_of_doom")), 0)

        with self.assertRaises(ValueError):
            get_binary_path(str())

        with self.assertRaises(ValueError):
            get_binary_path(None)

    def test_get_boot_datetime_as_timestamp_with_unknown_file(self):
        with self.assertRaises(FileNotFoundError):
            get_boot_datetime_as_timestamp("unknown-path")

        with self.assertRaises(FileNotFoundError):
            get_boot_datetime_as_timestamp(self.path)

    def test_get_boot_datetime_as_timestamp(self):
        uptime_path = self.path.joinpath("uptime")
        uptime_path.touch()

        self.assertIsNone(get_boot_datetime_as_timestamp(self.path))

        with uptime_path.open("w") as pipe:
            pipe.write("1337.0 42.0")

        result = get_boot_datetime_as_timestamp(self.path)
        self.assertIsNotNone(result)

        self.assertAlmostEqual(
            result, datetime.now().timestamp() - 1337.0, delta=1
        )

    def test_get_creation_datetime_from_file(self):
        creation_date = get_creation_datetime(Path("test", "test_utils.py"))
        self.assertIs(type(creation_date), datetime)

    def test_get_creation_datetime_from_unknown_file(self):
        creation_date = get_creation_datetime("unknown_file.txt")
        self.assertIsNone(creation_date)

    def test_glob_escape(self):
        self.assertEqual(glob_escape("geode-lib"), "geode-lib")
        self.assertEqual(glob_escape("geode-lib *"), "geode-lib [*]")
        self.assertEqual(glob_escape("geode-lib ?_*"), "geode-lib [?]_[*]")

        self.assertEqual(glob_escape("geode-lib [!]"), "geode-lib [[]![]]")

        self.assertEqual(
            glob_escape("geode-lib [E][!]"), "geode-lib [[]E[]][[]![]]"
        )

        self.assertEqual(
            glob_escape("geode-lib [[E]]"), "geode-lib [[][[]E[]][]]"
        )

    def test_parse_string_as_timedelta(self):
        with self.assertRaises(TypeError):
            parse_string_as_timedelta(None)

        with self.assertRaises(TypeError):
            parse_string_as_timedelta("", None)

        with self.assertRaises(ValueError):
            parse_string_as_timedelta("00:00")

        self.assertEqual(parse_string_as_timedelta("00:00:00"), timedelta())
        self.assertEqual(
            parse_string_as_timedelta("00:00:30"), timedelta(seconds=30)
        )
        self.assertEqual(
            parse_string_as_timedelta("24:00:00"), timedelta(seconds=3600 * 24)
        )
        self.assertEqual(
            parse_string_as_timedelta("48:00:00"), timedelta(days=2)
        )
        self.assertEqual(
            parse_string_as_timedelta("32088:00:42"),
            timedelta(days=1337, seconds=42),
        )

    def test_parse_timedelta_as_string(self):
        with self.assertRaises(TypeError):
            parse_timedelta_as_string(None)

        self.assertEqual(parse_timedelta_as_string(timedelta()), "00:00:00")
        self.assertEqual(
            parse_timedelta_as_string(timedelta(seconds=30)), "00:00:30"
        )
        self.assertEqual(
            parse_timedelta_as_string(timedelta(seconds=3600 * 24)), "24:00:00"
        )
        self.assertEqual(
            parse_timedelta_as_string(timedelta(days=2)), "48:00:00"
        )
        self.assertEqual(
            parse_timedelta_as_string(timedelta(days=1337, seconds=42)),
            "32088:00:42",
        )
