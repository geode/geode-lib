# ------------------------------------------------------------------------------
#  Copyleft 2021  PacMiam
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Geode
from geode_lib.filesystem import File
from test.utils import TestCaseMixin


class FileTestCase(TestCaseMixin):
    def test_directory_object(self):
        path = File(self.path)
        self.assertEqual(path.basename, "geode-lib-unittests")
        self.assertEqual(path.dirname, self.path.parent)

    def test_file_object(self):
        path = File(self.path.joinpath("file.txt"))
        self.assertEqual(path.basename, "file.txt")
        self.assertEqual(path.dirname, self.path)

    def test_file_object_stat(self):
        file_path = File(self.path.joinpath("file.txt"))
        self.assertFalse(file_path.is_executable())
        self.assertFalse(file_path.is_readable())
        self.assertFalse(file_path.is_writable())

        file_path.path.touch()
        self.assertFalse(file_path.is_executable())
        self.assertTrue(file_path.is_readable())
        self.assertTrue(file_path.is_writable())

        file_path.path.chmod(0o700)
        self.assertTrue(file_path.is_executable())
        self.assertTrue(file_path.is_readable())
        self.assertTrue(file_path.is_writable())
