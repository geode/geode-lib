# ------------------------------------------------------------------------------
#  Copyleft 2021  PacMiam
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Python
from pathlib import Path

# Geode
from geode_lib.collection import Collection, FilterableCollection
from test.utils import TestCaseMixin


class CollectionTestCase(TestCaseMixin):
    def test_collection_without_class_collision(self):
        collection = Collection(self.path)
        collection.recursive = True
        self.assertEqual(collection.recursive, True)

        collection2 = Collection(self.path)
        self.assertEqual(collection2.recursive, False)

    def test_empty_collection(self):
        collection = Collection(self.path)
        self.assertEqual(len(collection), 0)

    def test_not_empty_collection(self):
        for index in range(0, 10):
            self.path.joinpath(f"file_{index}.txt").touch()

        collection = Collection(self.path)
        self.assertEqual(len(collection), 10)

        self.path.joinpath("file_1.txt").unlink()
        self.assertEqual(len(collection), 9)

    def test_iterate_collection(self):
        for index in range(0, 10):
            self.path.joinpath(f"file_{index}.txt").touch()

        collection = Collection(self.path)
        for index, filename in enumerate(sorted(collection)):
            self.assertEqual(filename.name, f"file_{index}.txt")

    def test_patterned_files_collection(self):
        for filename in ("file.txt", "file.csv", "file.rst"):
            self.path.joinpath(filename).touch()

        collection = Collection(self.path)
        self.assertEqual(len(collection), 3)
        self.assertEqual(len(list(collection.files(pattern="*.rst"))), 1)

    def test_recursive_collection(self):
        for index in range(0, 5):
            self.path.joinpath(f"file_{index}.txt").touch()

        collection = Collection(self.path)
        collection.recursive = True
        self.assertEqual(len(collection), 5)

        subdirectory = self.path.joinpath("subdirectory")
        subdirectory.mkdir()

        subdirectory.joinpath("new_file.txt").touch()
        self.assertEqual(len(collection), 6)

    def test_toggle_recursive_flag(self):
        collection = Collection(self.path)
        self.assertFalse(collection.recursive)

        collection.toggle_recursive()
        self.assertTrue(collection.recursive)

    def test_unknown_collection(self):
        collection = Collection(self.path.joinpath("noop"))

        with self.assertRaises(FileNotFoundError):
            len(collection)

    def test_collection_raising_errors(self):
        collection = Collection(self.path)

        with self.assertRaises(TypeError):
            collection.recursive = None


class FilterableCollectionTestCase(TestCaseMixin):
    def test_collection_without_class_collision(self):
        collection = FilterableCollection(self.path)
        collection.add_allowed_extension(".txt")
        self.assertEqual(len(collection.allowed_extensions), 1)

        collection2 = FilterableCollection(self.path)
        self.assertEqual(len(collection2.allowed_extensions), 0)

    def test_collection_with_allowed_extensions(self):
        self.path.joinpath("file.txt").touch()
        self.path.joinpath("file.rst").touch()

        collection = FilterableCollection(self.path)
        self.assertEqual(len(collection), 2)

        collection.add_allowed_extension(".txt")
        self.assertEqual(len(collection), 1)

        collection.add_allowed_extension(".rst")
        self.assertEqual(len(collection), 2)

        self.assertCountEqual(collection.allowed_extensions, [".txt", ".rst"])

    def test_check_is_allowed_extension(self):
        collection = FilterableCollection(self.path)
        self.assertTrue(collection.is_ext_allowed(Path("file.txt")))

        collection.add_allowed_extension(".txt")
        self.assertTrue(collection.is_ext_allowed(Path("file.txt")))

        collection.add_allowed_extension(".rst")
        collection.remove_allowed_extension(".txt")
        self.assertFalse(collection.is_ext_allowed(Path("file.txt")))

    def test_check_is_ignored_file(self):
        collection = FilterableCollection(self.path)
        self.assertFalse(collection.is_ignored(Path("file.txt")))

        collection.add_ignored_pattern(r"\w+\.txt")
        self.assertTrue(collection.is_ignored(Path("file.txt")))

        collection.add_ignored_pattern(r"\w+\.rst")
        self.assertTrue(collection.is_ignored(Path("file.txt")))

        collection.remove_ignored_pattern(r"\w+\.txt")
        self.assertFalse(collection.is_ignored(Path("file.txt")))

    def test_collection_with_ignored_files(self):
        self.path.joinpath("file.txt").touch()
        self.path.joinpath("file.rst").touch()

        collection = FilterableCollection(self.path)
        self.assertEqual(len(collection), 2)

        collection.add_ignored_pattern(r"\w+\.txt")
        self.assertEqual(len(collection), 1)

        collection.add_ignored_pattern(r"\w+\.rst")
        self.assertEqual(len(collection), 0)

        self.assertCountEqual(
            collection.ignored_patterns, [r"\w+\.txt", r"\w+\.rst"]
        )

    def test_collection_raising_errors(self):
        collection = FilterableCollection(self.path)

        with self.assertRaises(TypeError):
            collection.add_allowed_extension(None)
        with self.assertRaises(TypeError):
            collection.add_ignored_pattern(None)

        with self.assertRaises(TypeError):
            collection.is_ext_allowed("Noop")
        with self.assertRaises(TypeError):
            collection.is_ignored("Noop")

        with self.assertRaises(TypeError):
            collection.remove_allowed_extension(None)
        with self.assertRaises(TypeError):
            collection.remove_ignored_pattern(None)
