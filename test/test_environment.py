# ------------------------------------------------------------------------------
#  Copyleft 2021  PacMiam
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Python
from unittest import TestCase

# Geode
from geode_lib.environment import Environment


class EnvironmentTestCase(TestCase):
    def test_empty_environment(self):
        environ = Environment()
        self.assertEqual(len(environ.environment), 0)

    def test_get_environment_variable(self):
        environ = Environment()

        for index in range(0, 5):
            environ.putenv(f"VAR_{index}", index)

        self.assertEqual(environ.getenv("VAR_1"), "1")

        self.assertIsNone(environ.getenv("VAR_42"))

    def test_not_empty_environment(self):
        environ = Environment(PATH=".", PWD=".")
        self.assertEqual(len(environ.environment), 2)

    def test_put_environment_variable(self):
        environ = Environment()

        for index in range(0, 5):
            environ.putenv(f"VAR_{index}", index)

        self.assertEqual(len(environ.environment), 5)
        self.assertEqual(environ.environment.get("VAR_4"), "4")

    def test_unset_environment_variable(self):
        environ = Environment()

        for index in range(0, 5):
            environ.putenv(f"VAR_{index}", index)

        environ.unsetenv("VAR_2")
        self.assertEqual(len(environ.environment), 4)

        with self.assertRaises(KeyError):
            environ.unsetenv("VAR_42")
