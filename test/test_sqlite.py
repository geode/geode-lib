# ------------------------------------------------------------------------------
#  Copyleft 2021  PacMiam
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Python
from pathlib import Path
from sqlite3 import OperationalError
from tempfile import gettempdir
from unittest import TestCase

# Geode
from geode_lib.sqlite import escape_request, parse_where, SqliteDatabase


class SqliteUtilsTestCase(TestCase):
    def test_escape_request(self):
        self.assertEqual(escape_request("key1"), '"key1"')

        self.assertEqual(
            escape_request("key1", "key2", "key3"), '"key1","key2","key3"'
        )

        self.assertEqual(
            escape_request("key1", "key2", "key3", separator="-"),
            '"key1"-"key2"-"key3"',
        )

        self.assertEqual(
            escape_request(key1="value1", key2="value2"),
            '"key1"="value1","key2"="value2"',
        )

        self.assertEqual(
            escape_request("key1", key2="value2"), '"key1","key2"="value2"'
        )

        self.assertEqual(escape_request(None), '"None"')

        self.assertEqual(escape_request(key1=None), '"key1"="None"')

    def test_escape_request_with_joker(self):
        self.assertEqual(escape_request("*"), "*")

        self.assertEqual(escape_request("key1", "*", "key3"), '"key1",*,"key3"')

    def test_parse_where(self):
        keys, values = parse_where(())
        self.assertIs(type(keys), tuple)
        self.assertEqual(len(keys), 0)
        self.assertIs(type(values), tuple)
        self.assertEqual(len(values), 0)

        keys, values = parse_where(("id = 0", "name = hop"))
        self.assertCountEqual(keys, ("id=?", "name=?"))
        self.assertCountEqual(values, ("0", "hop"))

        with self.assertRaises(ValueError):
            parse_where(("id 0"))


class SqliteTestCase(TestCase):
    def setUp(self):
        self.database = SqliteDatabase(":memory:")

    def create_dummy_database(self):
        self.database.create_table("test_table", "id integer", "name text")

        for index in range(0, 10):
            self.database.insert(
                "test_table", dict(id=index, name=f"user{index}")
            )

    def test_alter_table_with_add_column(self):
        self.create_dummy_database()

        with self.assertRaises(OperationalError):
            self.database.alter_table("test_table", add_column="name")

        self.database.alter_table("test_table", add_column="score")

        self.assertEqual(len(self.database.table_info("test_table")), 3)

    def test_alter_table_with_drop_column(self):
        self.create_dummy_database()

        with self.assertRaises(OperationalError):
            self.database.alter_table("test_table", drop_column="foo")

        self.database.alter_table("test_table", drop_column="name")

        self.assertEqual(len(self.database.table_info("test_table")), 1)

    def test_alter_table_with_rename_column(self):
        self.create_dummy_database()

        with self.assertRaises(OperationalError):
            self.database.alter_table(
                "test_table", rename_column=("unknown", "foo")
            )

        self.database.alter_table("test_table", rename_column=("name", "foo"))

        self.assertEqual(len(self.database.select("test_table", "*")), 10)

    def test_alter_table_with_rename_table(self):
        self.create_dummy_database()

        with self.assertRaises(OperationalError):
            self.database.alter_table("bar", rename_table="foo")

        self.database.alter_table("test_table", rename_table="foo")

        with self.assertRaises(OperationalError):
            self.database.execute("SELECT * FROM test_table;")

        self.assertEqual(len(self.database.select("foo", "*")), 10)

    def test_backup(self):
        self.create_dummy_database()

        path = Path(gettempdir()).joinpath("backup.db")
        self.database.backup(path)

        database = SqliteDatabase(path)
        self.assertEqual(len(database.select("test_table", "*")), 10)

    def test_delete(self):
        with self.assertRaises(OperationalError):
            self.database.delete("test_table")

        self.create_dummy_database()

        self.database.delete("test_table", where=["id = 1"])
        self.assertEqual(len(self.database.select("test_table", "*")), 9)

        self.database.delete("test_table")
        self.assertEqual(len(self.database.select("test_table", "*")), 0)

    def test_drop_table(self):
        self.database.drop_table("test_table")

        with self.assertRaises(OperationalError):
            self.database.drop_table("test_table", if_exists=False)

        self.create_dummy_database()

        self.database.drop_table("test_table")

        with self.assertRaises(OperationalError):
            self.database.execute("SELECT * FROM test_table;")

    def test_execute(self):
        with self.assertRaises(OperationalError):
            self.database.execute("SELECT * FROM test_table;")

        self.database.execute(
            "INSERT INTO test_table(id, name) VALUES(?, ?)", (0, "foo")
        )

        self.create_dummy_database()
        print(self.database.execute("SELECT * FROM test_table;"))

        self.assertIsNone(
            self.database.execute(
                "INSERT INTO test_table(id, name) VALUES(?, ?)", (10, "foo")
            )
        )

    def test_select(self):
        self.create_dummy_database()

        rows = self.database.select("test_table", "*")
        self.assertEqual(len(rows), 10)
        self.assertEqual(rows[0]["name"], "user0")

        rows = self.database.select("test_table", "*", where=["id = 4"])
        self.assertEqual(len(rows), 1)
        self.assertEqual(rows[0]["name"], "user4")

        rows = self.database.select(
            "test_table", "*", where=["id = 2", "name = 'user6'"]
        )
        self.assertEqual(len(rows), 0)

    def test_table_info(self):
        self.assertEqual(len(self.database.table_info("test_table")), 0)

        self.create_dummy_database()

        rows = self.database.table_info("test_table")
        self.assertEqual(rows[0]["name"], "id")
        self.assertEqual(rows[0]["type"], "INTEGER")
        self.assertEqual(rows[1]["name"], "name")
        self.assertEqual(rows[1]["type"], "TEXT")

    def test_update(self):
        with self.assertRaises(TypeError):
            self.database.update("foo", dict(name="bar"), where="id = 0")

        with self.assertRaises(OperationalError):
            self.database.update("foo", dict(name="bar"), where=["id = 0"])

        self.create_dummy_database()

        self.database.update("test_table", dict(name="bar"), where=["id = 0"])

        rows = self.database.select("test_table", "name", where=["id = 0"])
        self.assertEqual(rows[0]["name"], "bar")

    def test_update_with_manual_commit(self):
        self.create_dummy_database()

        self.database.update(
            "test_table", dict(name="bar"), where=["id = 0"], commit=False
        )

        rows = self.database.select("test_table", "name", where=["id = 0"])
        self.assertEqual(rows[0]["name"], "bar")

        self.database.commit()

        rows = self.database.select("test_table", "name", where=["id = 0"])
        self.assertEqual(rows[0]["name"], "bar")

    def test_update_with_rollback(self):
        self.create_dummy_database()

        self.database.update(
            "test_table", dict(name="bar"), where=["id = 0"], commit=False
        )

        rows = self.database.select("test_table", "name", where=["id = 0"])
        self.assertEqual(rows[0]["name"], "bar")

        self.database.rollback()

        rows = self.database.select("test_table", "name", where=["id = 0"])
        self.assertEqual(rows[0]["name"], "user0")

    def test_version(self):
        self.assertRegex(self.database.version, r"\d+\.\d+\.\d+")
