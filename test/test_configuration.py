# ------------------------------------------------------------------------------
#  Copyleft 2021  PacMiam
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Geode
from geode_lib.configuration import Configuration
from test.utils import TestCaseMixin


class ConfigurationTestCase(TestCaseMixin):
    def test_constructor_with_unknown_file(self):
        configuration = Configuration(self.path.joinpath("unknown_file.ini"))
        self.assertFalse(configuration.path.exists())
        self.assertEqual(len(configuration.sections()), 0)

    def test_constructor_with_empty_file(self):
        path = self.path.joinpath("config.ini")
        path.touch()

        configuration = Configuration(path)
        self.assertTrue(configuration.path.exists())
        self.assertEqual(len(configuration.sections()), 0)

    def test_constructor_with_configuration_file(self):
        path = self.path.joinpath("config.ini")

        with path.open("w") as pipe:
            pipe.write("[section]\noption1 = value\noption2 = yes")

        configuration = Configuration(path)
        self.assertTrue(configuration.path.exists())
        self.assertCountEqual(configuration.sections(), ["section"])

        self.assertEqual(configuration.get("section", "option1"), "value")
        with self.assertRaises(ValueError):
            configuration.getboolean("section", "option1")

        self.assertEqual(configuration.get("section", "option2"), "yes")
        self.assertEqual(configuration.getboolean("section", "option2"), True)

    def test_save_with_data(self):
        path = self.path.joinpath("config.ini")

        configuration = Configuration(path)
        configuration.add_section("test")
        configuration.set("test", "foo", "bar")
        configuration.save()
        self.assertTrue(path.exists())

        with path.open("r") as pipe:
            self.assertEqual(pipe.read(), "[test]\nfoo = bar\n\n")

    def test_save_with_not_empty_file(self):
        path = self.path.joinpath("config.ini")
        with path.open("w") as pipe:
            pipe.write("This is not an empty file :)")

        self.assertTrue(path.exists())

        configuration = Configuration()
        with self.assertRaises(FileExistsError):
            configuration.save(path)

        configuration.save(path, force_if_exists=True)
        self.assertTrue(path.exists())

        with path.open("r") as pipe:
            self.assertEqual(pipe.read(), "")

        configuration = Configuration(path)
        configuration.save()

        path = self.path.joinpath("config2.ini")
        path.touch()

        configuration.add_section("test")
        configuration.set("test", "alice", "bob")
        configuration.save(str(path), force_if_exists=True)

    def test_save_with_unknown_file(self):
        path = self.path.joinpath("unknown_file.ini")

        configuration = Configuration(path)
        configuration.save()
        self.assertTrue(path.exists())
        path.unlink()

        configuration = Configuration(str(path))
        configuration.save()
        self.assertTrue(path.exists())
        path.unlink()

    def test_save_with_wrong_file_type(self):
        configuration = Configuration()

        with self.assertRaises(IsADirectoryError):
            configuration.save()
