# ------------------------------------------------------------------------------
#  Copyleft 2021  PacMiam
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Python
from unittest import TestCase

# Geode
from geode_lib.executable import Executable


class ExecutableTestCase(TestCase):
    def test_basic_executable(self):
        executable = Executable("ls")

        output = executable.run(capture_output=True)
        self.assertIsNotNone(output)
        self.assertIn(b"geode_lib\n", output.stdout)

        output = executable.run("-la", capture_output=True)
        self.assertIsNotNone(output)
        self.assertIn(b"geode_lib\n", output.stdout)

    def test_basic_executable_with_wrong_arguments(self):
        executable = Executable("ls")
        with self.assertRaises(TypeError):
            executable.arguments = "-la"

        with self.assertRaises(TypeError):
            executable.arguments = None

    def test_basic_executable_with_correct_arguments(self):
        executable = Executable("ls")
        executable.arguments = ["-l", "-a"]

        output = executable.run(capture_output=True)
        self.assertIsNotNone(output)
        self.assertIn(b"geode_lib\n", output.stdout)

    def test_unknown_executable(self):
        executable = Executable()

        with self.assertRaises(PermissionError):
            executable.run()
