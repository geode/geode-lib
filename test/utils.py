# ------------------------------------------------------------------------------
#  Copyleft 2021  PacMiam
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Python
from pathlib import Path
from shutil import rmtree
from tempfile import gettempdir
from unittest import TestCase


class TestCaseMixin(TestCase):
    def setUp(self):
        """Initialize test environment with some data"""

        self.path = Path(gettempdir(), "geode-lib-unittests")
        self.path.mkdir()

    def tearDown(self):
        """Remove dedicated test environment data"""

        if self.path.exists():
            rmtree(self.path)
