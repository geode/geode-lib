#  -----------------------------------------------------------------------------
#  Copyleft 2021-2023  PacMiam
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  -----------------------------------------------------------------------------

import nox


@nox.session(reuse_venv=True, tags=["check", "style"])
def flake8(session):
    session.install("flake8")
    session.run(
        "flake8",
        "--max-line-length=80",
        "--ignore=E402,W503",
        "geode_lib",
        "test",
    )


@nox.session(reuse_venv=True, tags=["check", "style"])
def black(session):
    session.install("black")
    session.run(
        "black",
        "--check",
        "--diff",
        "--color",
        "--line-length=80",
        "geode_lib",
        "noxfile.py",
        "test",
    )


@nox.session(reuse_venv=True, tags=["style"])
def black_apply(session):
    session.install("black")
    session.run("black", "--line-length=80", "geode_lib", "noxfile.py", "test")


@nox.session(reuse_venv=True)
@nox.parametrize("python", ["3.11"])
def tests(session):
    session.install(".[dev]")
    try:
        session.run("coverage", "run", "-m", "pytest", *session.posargs)
    finally:
        session.run("coverage", "html")
