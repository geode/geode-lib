# ------------------------------------------------------------------------------
#  Copyleft 2021-2022  PacMiam
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Python
from pathlib import Path
from os import access, R_OK, W_OK, X_OK

# Geode
from geode_lib.environment import Environment


class File(Environment):
    """Represents a file object on filesystem with a dedicated environment

    See Also
    --------
    geode_lib.environment.Environment
    """

    def __init__(self, *args, **kwargs):
        """Constructor"""

        Environment.__init__(self, **kwargs)

        self.path = Path(*args)

    @property
    def basename(self) -> str:
        """Retrieve the file basename

        Returns
        -------
        str
            File basename as string

        See Also
        --------
        pathlib.Path.name
        """

        return self.path.name

    @property
    def dirname(self) -> str:
        """Retrieve the file dirname

        Returns
        -------
        str
            File dirname as string

        See Also
        --------
        pathlib.Path.parent
        """

        return self.path.parent

    def exists(self) -> bool:
        """Check if the file exists on filesystem

        Returns
        -------
        bool
            True if file exists, False otherwise

        See Also
        --------
        pathlib.Path.exists()
        """

        return self.path.exists()

    def is_executable(self) -> bool:
        """Retrieve the executable status

        Returns
        -------
        bool
            True if the current file is executable, False otherwise

        See Also
        --------
        os.access
        os.X_OK
        """

        return access(self.path, X_OK)

    def is_readable(self) -> bool:
        """Retrieve the readable status

        Returns
        -------
        bool
            True if the current file is readable, False otherwise

        See Also
        --------
        os.access
        os.R_OK
        """

        return access(self.path, R_OK)

    def is_writable(self) -> bool:
        """Retrieve the writable status

        Returns
        -------
        bool
            True if the current file is writable, False otherwise

        See Also
        --------
        os.access
        os.W_OK
        """

        return access(self.path, W_OK)
