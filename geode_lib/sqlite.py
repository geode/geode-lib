# ------------------------------------------------------------------------------
#  Copyleft 2021  PacMiam
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Python
import sqlite3


def escape_request(*args, separator=",", **kwargs):
    """Retrieve an escaped string which can be used for Sqlite request

    Parameters
    ----------
    separator : str, default: ,
        Character used to separate arguments

    Returns
    -------
    str
        Escaped string

    Examples
    --------
    >>> escape_request('name', 'type')
    '"name","type"'

    >>> escape_request(name='test')
    '"name"="test"'
    """

    request = list()

    if args:
        for element in args:
            request.append(f'"{element}"' if not element == "*" else "*")

    if kwargs:
        request.extend([f'"{key}"="{value}"' for key, value in kwargs.items()])

    return separator.join(request)


def parse_where(where_conditions):
    """Parse where conditions to use the DB-API’s parameter substitution

    Parameters
    ----------
    where_conditions : tuple
        SQL where conditions as tuple of string

    Returns
    -------
    tuple, tuple
        Parsed SQL Where statement as tuple of key and tuple of value

    Examples
    --------
    >>> parse_where(("name = foo", "id = 1"))
    (('name=?', 'id=?'), ('foo', '1'))
    """

    keys, values = tuple(), tuple()

    for element in where_conditions:
        key, value = element.split("=")

        keys += (f"{key.strip()}=?",)
        values += (value.strip(),)

    return keys, values


class SqliteDatabase:
    """Represents a minimalist Sqlite database implementation

    Attributes
    ----------
    database : str or pathlib.Path
        Sqlite database pathname
    arguments : dict
        Optional arguments sent to sqlite3.connect method
    connection : sqlite3.Connection, default: None
        Sqlite database connection instance

    See Also
    --------
    sqlite3
    """

    def __init__(self, database, **kwargs):
        """Constructor

        Parameters
        ----------
        database : pathlib.Path or str
            The Sqlite database path on filesystem. Use ':memory' to open a
            database that resides in RAM instead on filesystem.
        kwargs : dict
            Optional arguments send to the Sqlite connect method

        See Also
        --------
        sqlite3.connect

        Examples
        --------
        >>> SqliteDatabase("/tmp/foo.db")

        >>> SqliteDatabase(":memory:", isolation_level=None)
        """

        self.database = database
        self.arguments = kwargs

        self.connection = None

    def __del__(self):
        """Close database instance when object is deleted"""

        self.close()

    def close(self):
        """Close the database connection

        See Also
        --------
        sqlite3.Connection.close
        """

        if self.connection is not None:
            self.connection.close()

            self.connection = None

    def connect(function):
        """Decorator which open a database connection for specified function

        See Also
        --------
        sqlite3.connect
        """

        def __function__(self, *args, **kwargs):
            if self.connection is None:
                self.connection = sqlite3.connect(
                    self.database, **self.arguments
                )

            return function(self, *args, **kwargs)

        return __function__

    @connect
    def backup(self, backup_path, **kwargs):
        """Backup the current Sqlite database to another location

        Parameters
        ----------
        backup_path : pathlib.Path or str
            The backup Sqlite database path

        See Also
        --------
        sqlite3.Connection.backup

        Examples
        --------
        >>> db = SqliteDatabase("/tmp/foo.db")
        >>> db.backup("/tmp/bar.db")

        >>> db = SqliteDatabase("/tmp/foo.db")
        >>> db.backup("/tmp/baz.db", pages=1, progress=lambda x: print(x))
        """

        with sqlite3.connect(backup_path) as backup_connection:
            self.connection.backup(backup_connection, **kwargs)

    @connect
    def commit(self):
        """Commit the current transaction if necessary

        See Also
        --------
        sqlite3.Connection.commit
        sqlite3.Connection.in_transaction
        """

        if self.connection.in_transaction:
            self.connection.commit()

    @connect
    def execute(self, request, args=None, commit=True):
        """Open a new connection and execute the specified SQL request

        Parameters
        ----------
        request : str
            SQL request to execute in a Sqlite database cursor
        commit : bool, default: True
            Commit the current transaction if there is any changes

        Returns
        -------
        list
            When the specified request returns some data

        See Also
        --------
        sqlite3.Connection.commit
        sqlite3.Connection.execute
        sqlite3.Cursor.fetchall

        Examples
        --------
        >>> db = SqliteDatabase("/tmp/foo.db")
        >>> db.execute("SELECT * FROM foo;")
        [<sqlite3.Row object at 0x7fd8bc616c50>]
        """

        if sqlite3.complete_statement(request):
            # TODO: Allow to customize the row_factory
            self.connection.row_factory = sqlite3.Row

            # Detect the current SQL request scope
            request_type = request.split()[0].strip().upper()

            if isinstance(args, (list, tuple)):
                cursor = self.connection.execute(request, args)
            else:
                cursor = self.connection.execute(request)

            if request_type in ("SELECT", "PRAGMA"):
                return cursor.fetchall()

            if self.connection.in_transaction and commit:
                self.connection.commit()

    @connect
    def rollback(self):
        """Rolls back any changes from the database since the last commit

        See Also
        --------
        sqlite3.Connection.in_transaction
        sqlite3.Connection.rollback
        """

        if self.connection.in_transaction:
            self.connection.rollback()

    def alter_table(
        self,
        table,
        rename_table=None,
        rename_column=None,
        add_column=None,
        drop_column=None,
        constraints=None,
    ):
        """Alter a specific table from current database

        https://www.sqlite.org/lang_altertable.html

        Parameters
        ----------
        table : str
            The table name in current database
        rename_table : str, default: None
            The new table name
        rename_column : tuple, default: None
            Tuple which contains old and new column names
        add_column : str, default: None
            The new column name to add in the specified table
        drop_column : str, default: None
            The column name to drop from the specified table
        constraints : str, default: None
            The column constraints used with the add_column argument

        Examples
        --------
        >>> db = SqliteDatabase(":memory:")
        >>> db.alter_table("foo", rename_table="bar")

        >>> db = SqliteDatabase(":memory:")
        >>> db.alter_table("foo", rename_column=("baz", "qux"))

        >>> db = SqliteDatabase(":memory:")
        >>> db.alter_table("foo", add_column="baz")
        """

        request = f'ALTER TABLE "{table}"'

        if isinstance(rename_table, str):
            request += f' RENAME TO "{rename_table}"'

        if isinstance(rename_column, (list, tuple)):
            old, new = rename_column
            request += f' RENAME COLUMN "{old}" TO "{new}"'

        if isinstance(add_column, str):
            request += f' ADD COLUMN "{add_column}"'
            if isinstance(constraints, str):
                request += constraints

        if isinstance(drop_column, str):
            request += f' DROP COLUMN "{drop_column}"'

        self.execute(f"{request};")

    def create_table(self, table, *args):
        """Create a table in current database with a specific schema

        https://www.sqlite.org/lang_createtable.html

        Parameters
        ----------
        table : str
            The table name in current database
        args : list
            The schema as a list of string

        Examples
        --------
        >>> db = SqliteDatabase(":memory:")
        >>> db.create_table("foo", "id integer", "name text")
        """

        self.execute(f"CREATE TABLE \"{table}\" ({', '.join(args)});")

    def delete(self, table, where=None):
        """Delete a subset of data from a specific table

        https://www.sqlite.org/lang_delete.html

        Parameters
        ----------
        table : str
            The table name in current database
        where : list or tuple, default: None
            The where statement structure used to retrieve specific values

        Examples
        --------
        >>> db = SqliteDatabase(":memory:")
        >>> db.delete("foo")

        >>> db = SqliteDatabase(":memory:")
        >>> db.delete("foo", where=["name = bar"])
        """

        request = f'DELETE FROM "{table}"'

        if isinstance(where, (list, tuple)) and len(where) > 0:
            keys, values = parse_where(where)

            self.execute(f"{request} WHERE {' AND '.join(keys)};", values)

        else:
            self.execute(f"{request};")

    def drop_table(self, table, if_exists=True):
        """Remove a specific table from current database

        https://www.sqlite.org/lang_droptable.html

        Parameters
        ----------
        table : str
            The table name in current database
        if_exists : bool, default: True
            If True, disable the transaction error when the table already exists

        Examples
        --------
        >>> db = SqliteDatabase(":memory:")
        >>> db.drop_table("foo")
        """

        self.execute(f"DROP TABLE {'IF EXISTS ' if if_exists else ''}{table};")

    def insert(self, table, data, commit=True):
        """Insert a new data in a specific table

        https://www.sqlite.org/lang_insert.html

        Parameters
        ----------
        table : str
            The table name in current database
        data : dict
            The data to insert with table column as key
        commit : bool, default: True
            If True, commit the current transaction

        Examples
        --------
        >>> db = SqliteDatabase(":memory:")
        >>> db.insert("foo", {"id": 10, "name": "alice"})

        >>> db = SqliteDatabase(":memory:")
        >>> db.insert("foo", {"id": 42, "name": "bob"}, commit=False)
        >>> db.rollback()
        """

        keys = escape_request(*data.keys())
        values = ",".join("?" * len(data))

        self.execute(
            f'INSERT INTO "{table}" ({keys}) VALUES({values});',
            tuple(data.values()),
            commit=commit,
        )

    def select(self, table, *args, where=None):
        """Retrieve a subset of data from a specific table

        https://www.sqlite.org/lang_select.html

        Parameters
        ----------
        table : str
            The table name in current database
        where : list or tuple, default: None
            The where statement structure used to retrieve specific values

        Returns
        -------
        list
            The request result as a list of sqlite3.Row

        Examples
        --------
        >>> db = SqliteDatabase(":memory:")
        >>> db.select("foo", '*')
        [<sqlite3.Row object at 0x865de2fcb3ad>,
         <sqlite3.Row object at 0x48def62cbda3>]

        >>> db = SqliteDatabase(":memory:")
        >>> db.select("foo", '*', where=["name = baz"])
        [<sqlite3.Row object at 0x865de2fcb3ad>]
        """

        request = f'SELECT {escape_request(*args)} FROM "{table}"'

        if isinstance(where, (list, tuple)) and len(where) > 0:
            keys, values = parse_where(where)

            return self.execute(
                f"{request} WHERE {' AND '.join(keys)};", values
            )

        return self.execute(f"{request};")

    def table_info(self, table):
        """Retrieve column information for the specified table

        https://www.sqlite.org/pragma.html#pragma_table_info

        Parameters
        ----------
        table : str
            The table name in current database

        Returns
        -------
        list
            The request result as a list of sqlite3.Row

        Examples
        --------
        >>> db = SqliteDatabase(":memory:")
        >>> db.table_info("foo")
        [<sqlite3.Row object at 0x145dfb367eda>]
        """

        return self.execute(f'PRAGMA table_info("{table}");')

    def update(self, table, data, where, commit=True):
        """Modify a subset of data from a specific table

        https://www.sqlite.org/lang_update.html

        Parameters
        ----------
        table : str
            The table name in current database
        data : dict
            The new values to set with table column as key
        where : list or tuple
            The where statement structure used to retrieve specific values
        commit : bool, default: True
            If True, commit the current transaction

        Examples
        --------
        >>> db = SqliteDatabase(":memory:")
        >>> db.update("foo", {"name": "bar"}, where=["name = baz"])
        """

        request = f'UPDATE "{table}" SET {escape_request(**data)}'

        if not isinstance(where, (list, tuple)):
            raise TypeError("The where argument type must be a list or a tuple")

        keys, values = parse_where(where)

        self.execute(
            f"{request} WHERE {' AND '.join(keys)};", values, commit=commit
        )

    @property
    def version(self):
        """Retrieve the version number of the run-time SQLite library

        Returns
        -------
        str
            The current version of the SQLite library

        Examples
        --------
        >>> db = SqliteDatabase(":memory:")
        >>> db.version
        3.37.0
        """

        return sqlite3.sqlite_version
