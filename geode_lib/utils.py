# ------------------------------------------------------------------------------
#  Copyleft 2021  PacMiam
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Python
from datetime import datetime, timedelta
from os import access, environ, R_OK
from os.path import getctime
from pathlib import Path
from re import sub


def are_equivalent_timestamps(first, second, delta=0):
    """Check if two timestamps are equivalent

    Parameters
    ----------
    first : int
        First timestamp
    second : int
        Second timestamp
    delta : int, optional
        Add a delta (in second) where the difference is negligible

    Returns
    -------
    bool
        True if timestamps are equivalent, False otherwise
    """

    return abs(int(float(first)) - int(float(second))) in range(0, delta + 1)


def generate_extension(extension):
    """Generate a regex pattern to check lower and upper case extensions

    Thanks to https://stackoverflow.com/a/10148272

    Parameters
    ----------
    extension : str
        Extension to parse without the first dot

    Returns
    -------
    str
        Regex pattern

    Examples
    --------
    >>> generate_extension("nes")
    '[nN][eE][sS]'
    """

    pattern = str()

    for character in extension:
        if not character == ".":
            pattern += f"[{character.lower()}{character.upper()}]"
        else:
            pattern += "."

    return pattern


def generate_identifier(path, use_inode=True):
    """Generate an identifier from a path

    Parameters
    ----------
    path : pathlib.Path or str
        Path to parse into indentifier
    use_inode : bool, default: True
        Add the file inode on filesystem at the end of the identifier

    Returns
    -------
    str
        Identifier string

    Examples
    --------
    >>> generate_identifier('Double Dragon (Europe).nes')
    'double-dragon-europe-nes-25953832'
    >>> generate_identifier('Double Dragon (Europe).nes', use_inode=False)
    'double-dragon-europe-nes'
    """

    if isinstance(path, str):
        path = Path(path).expanduser()

    # Retrieve only alphanumeric element from filename
    name = sub(r"[^\w\d]+", " ", path.name.lower())
    # Remove useless spaces and replace the others with a dash
    name = sub(r"[\s|_]+", "-", name.strip())

    # Retrieve file inode number
    if use_inode and path.exists():
        inode = path.stat().st_ino
        if inode > 0:
            name = f"{name}-{inode}"

    return name


def get_binary_path(binary):
    """Get a list of available binary paths from $PATH variable

    This function get all the path from $PATH variable which match binary
    request.

    Parameters
    ----------
    binary : str or pathlib.Path
        Binary name or path

    Returns
    -------
    list
        List of available path

    Raises
    ------
    ValueError
        When the specified binary argument is empty or null

    Examples
    --------
    >>> get_binary_path("ls")
    ['/bin/ls']
    """

    available = list()

    if binary is None or len(str(binary)) == 0:
        raise ValueError("binary argument must not be an empty string")

    if isinstance(binary, str):
        binary = Path(binary).expanduser()

    if binary.exists():
        available.append(binary.name)

    for directory in set(environ["PATH"].split(":")):
        path = Path(directory).expanduser()
        if not access(path, R_OK):
            continue

        binary_path = path.joinpath(binary)
        if binary_path.exists() and binary_path.name not in available:
            available.append(str(binary_path))

    return available


def get_boot_datetime_as_timestamp(proc_path="/proc"):
    """Retrieve boot datetime as timestamp

    The 'uptime' file contains two values: boot time and idle time. This method
    only retrieve the first one to calculate the boot datetime.

    Parameters
    ----------
    proc_path : pathlib.Path or str, optional
        Process file system path (Only used to test the method)

    Returns
    -------
    float
        Boot datetime as timestamp value

    Raises
    ------
    FileNotFoundError
        When the /proc directory do not exists on filesystem
        When the /proc/uptime file do not exists on filesystem
    """

    if isinstance(proc_path, str):
        proc_path = Path(proc_path)

    if not proc_path.exists():
        raise FileNotFoundError("Cannot found process file system")

    uptime_file = proc_path.joinpath("uptime")
    if not uptime_file.exists():
        raise FileNotFoundError(f"Cannot found {uptime_file} on filesystem")

    with uptime_file.open("r") as pipe:
        content = pipe.read().split("\n")[0]

    if content:
        return datetime.now().timestamp() - float(content.split()[0])

    return None


def get_creation_datetime(path):
    """Retrieve the creation date from a specific filename

    Parameters
    ----------
    path : pathlib.Path or str
        Path to retrieve creation datetime

    Returns
    -------
    datetime.datetime
        Creation datetime object

    Examples
    --------
    >>> get_creation_datetime("~/.bashrc")
    datetime.datetime(2019, 9, 22, 14, 1, 37, 56527)
    """

    if isinstance(path, str):
        path = Path(path).expanduser()

    if not path.exists():
        return None

    return datetime.fromtimestamp(getctime(path))


def glob_escape(string):
    """Generate a valid escaped string for glob pattern expansion

    The glob.escape method do not replace the ] character with []] and this
    replacement is mandatory to have a valid pattern for glob.

    With glob.escape: [A] → [[]A]
    With this method: [A] → [[]A[]]

    Parameters
    ----------
    string : str
        String to escape for glob processing

    Returns
    -------
    str
        Escaped string

    Examples
    --------
    >>> glob_escape("[E]")
    '[[]E[]]'
    >>> glob_escape("[E][!]")
    '[[]E[]][[]![]]'
    """

    strings = [
        ("[", "[["),
        ("]", "]]"),
        ("[[", "[[]"),
        ("]]", "[]]"),
        ("?", "[?]"),
        ("*", "[*]"),
    ]

    pattern = str(string)
    for replacer in strings:
        pattern = pattern.replace(*replacer)

    return pattern


def parse_string_as_timedelta(string, separator=":"):
    """Parse a string as a datetime timedelta object

    Parameters
    ----------
    string : str
        The string to parse into a timedelta object
    separator : str, default: ':'
        The character to use when spliting the specified string parameter

    Returns
    -------
    datetime.timedelta
        The parsed timedelta object based on specified parameters

    Raises
    ------
    TypeError
        When the specified string parameter is not a string
        When the specified separator parameter is not a string
    StringError
        When the specified string parameter do not contains the separator
        character twice (eg: HH:MM:SS)
    """

    if not isinstance(string, str):
        raise TypeError("The 'string' parameter must be a string")

    if not isinstance(separator, str):
        raise TypeError("The 'separator' parameter must be a string")

    if not string.count(separator) == 2:
        raise ValueError(
            "The specified 'string' parameter do not contains "
            "the separator character twice"
        )

    hours, minutes, seconds = string.split(separator)

    return timedelta(
        hours=int(hours), minutes=int(minutes), seconds=int(seconds)
    )


def parse_timedelta_as_string(delta):
    """Parse a timedelta to string

    Get a string from the timedelta formated as HH:MM:SS

    Parameters
    ----------
    delta : datetime.timedelta
        The timedelta to parse

    Returns
    -------
    str
        Parsed timedelta as formated string

    Raises
    ------
    TypeError
        When the specified delta parameter is not a datetime.timedelta object
    """

    if not isinstance(delta, timedelta):
        raise TypeError(
            "The 'delta' parameter must be a " "datetime.timedelta object"
        )

    hours, seconds = divmod(delta.seconds, 3600)
    if seconds > 0:
        minutes, seconds = divmod(seconds, 60)
    else:
        minutes = int()

    hours += delta.days * 24

    return f"{hours:02d}:{minutes:02d}:{seconds:02d}"
