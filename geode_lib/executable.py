# ------------------------------------------------------------------------------
#  Copyleft 2021-2022  PacMiam
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Python
from subprocess import run

# Geode
from geode_lib.filesystem import File


class Executable(File):
    """Represents an executable script or binary on filesystem

    Attributes
    ----------
    __arguments__ : list
        Optional arguments used in the command line

    See Also
    --------
    geode_lib.filesystem.File
    """

    def __init__(self, *args, **kwargs):
        """Constructor

        See Also
        --------
        geode_lib.file.File.__init__
        """

        super().__init__(*args, **kwargs)

        self.__arguments__ = list()

    @property
    def arguments(self) -> list:
        """Retrieve the optional arguments

        Returns
        -------
        list
            The list of arguments
        """

        return self.__arguments__

    @arguments.setter
    def arguments(self, value: list):
        """Define a new set of optional arguments

        Parameters
        ----------
        value : list
            The new list of arguments

        Raises
        ------
        TypeError
            When the specified value argument is not a list
        """

        if not isinstance(value, list):
            raise TypeError("The specified argument must be a list")

        self.__arguments__ = value

    @property
    def command_line(self) -> list:
        """Prepare executable command line

        Returns
        -------
        list
            Command line as list of string
        """

        command_line = [str(self.path)]
        command_line.extend(self.arguments)

        return command_line

    def run(self, *args, **kwargs):
        """Run executable with specified parameters

        Returns
        -------
        subprocess.CompletedProcess
            Finished process results

        See Also
        --------
        subprocess.run
        """

        command_line = self.command_line
        command_line.extend(args)

        if "env" not in kwargs:
            kwargs["env"] = self.environment

        return run(command_line, **kwargs)
