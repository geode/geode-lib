# ------------------------------------------------------------------------------
#  Copyleft 2021  PacMiam
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------


class Environment:
    """Represents an environment variables storage

    Attributes
    ----------
    environment : dict
        Environment variables storage as dictionary structure
    """

    def __init__(self, **kwargs):
        """Constructor"""

        self.environment = dict()

        for item in kwargs.items():
            self.putenv(*item)

    def getenv(self, key: str, default=None) -> str:
        """Retrieve the value of the environment variable key

        Parameters
        ----------
        key : str
            Environment variable key name
        default : str, default: None
            Fallback value if environment variable key is not available

        Returns
        -------
        str
            Environment variable value as string
        """

        return self.environment.get(key, default)

    def putenv(self, key: str, value: str):
        """Set the environment variable key with the specified value

        Parameters
        ----------
        key : str
            Environment variable key name
        value : str
            Value to use with specified environment variable key
        """

        self.environment[key] = str(value)

    def unsetenv(self, key: str):
        """Unset the environment value key

        Parameters
        ----------
        key : str
            Environment variable key name
        """

        del self.environment[key]
