# ------------------------------------------------------------------------------
#  Copyleft 2021-2022  PacMiam
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Python
from pathlib import Path
from re import match, IGNORECASE

# Geode
from geode_lib.filesystem import File


class Collection(File):
    """Represents a files collection on filesystem

    Attributes
    ----------
    __recursive__ : bool
        Flag used to activate recursive listing with Collection.files method

    See Also
    --------
    geode_lib.filesystem.File
    """

    def __init__(self, *args, **kwargs):
        """Constructor

        See Also
        --------
        geode_lib.collection.Collection.__init__
        """

        super().__init__(*args, **kwargs)

        self.__recursive__ = False

    def __len__(self):
        """Retrieve collection files length

        Returns
        -------
        int
            Files number in current collection
        """

        return len(list(self.files()))

    def __iter__(self):
        """Allows to use the collection as generator

        Returns
        -------
        generator
            Collection as generator object
        """

        return self.files()

    def files(self, pattern="*"):
        """Retrieve a collection files list

        Parameters
        ----------
        pattern : str, default: '*'
            Glob-style pattern used to select specific files in collection

        Raises
        ------
        FileNotFoundError
            When collection root file do not exists on filesystem

        Yields
        ------
        pathlib.Path
            Path object of the yield filepath
        """

        if not self.exists():
            raise FileNotFoundError(
                f"Cannot access to '{self.path}': No such file or directory"
            )

        if self.recursive:
            files = self.path.rglob(pattern)
        else:
            files = self.path.glob(pattern)

        for path in files:
            if path.is_file():
                yield path

    @property
    def recursive(self) -> bool:
        """Retrieve the recursive flag value from collection

        Returns
        -------
        bool
            The recursive flag value
        """

        return self.__recursive__

    @recursive.setter
    def recursive(self, value: bool):
        """Define a new value for the collection recursive flag

        Parameters
        ----------
        value : bool
            The new value to set as the recursive flag

        Raises
        ------
        TypeError
            When the specified value argument is not a boolean
        """

        if not isinstance(value, bool):
            raise TypeError("The specified argument must be a boolean")

        self.__recursive__ = value

    def toggle_recursive(self):
        """Switch the collection recursive flag value"""

        self.recursive = not self.recursive


class FilterableCollection(Collection):
    """Represents a filterable files collection on filesystem

    Attributes
    ----------
    __ignored_patterns__ : list
        List of regular expression patterns used to ignore specific file name
    __allowed_extensions__ : list
        List of allowed file extensions used to filter files list (An empty
        list means all extension are allowed)

    See Also
    --------
    geode_lib.collection.Collection
    """

    def __init__(self, *args, **kwargs):
        """Constructor

        See Also
        --------
        geode_lib.collection.Collection.__init__
        """

        super().__init__(*args, **kwargs)

        self.__allowed_extensions__ = list()
        self.__ignored_patterns__ = list()

    def files(self, **kwargs):
        """Retrieve a filtered collection files list

        Yields
        ------
        pathlib.Path
            Path object of the yield filepath

        See Also
        --------
        geode_lib.collection.Collection.files
        """

        files = super().files(**kwargs)

        for filename in files:
            if not self.is_ext_allowed(filename) or self.is_ignored(filename):
                continue

            yield filename

    def add_allowed_extension(self, extension: str):
        """Add an extension to the allowed extensions list

        Notes
        -----
        If the specified extension does exists in allowed extensions list, this
        method do nothing.

        Parameters
        ----------
        extension : str
            The extension to add into collection allowed extensions list

        Raises
        ------
        TypeError
            When the specified extension argument is not a string
        """

        if not isinstance(extension, str):
            raise TypeError("The specified extension argument must be a string")

        if extension not in self.__allowed_extensions__:
            self.__allowed_extensions__.append(extension)

    def add_ignored_pattern(self, pattern: str):
        """Add a pattern to the ignored patterns list

        Notes
        -----
        If the specified pattern does exists in ignored patterns list, this
        method do nothing.

        Parameters
        ----------
        extension : str
            The pattern to add into collection ignored patterns list

        Raises
        ------
        TypeError
            When the specified pattern argument is not a string
        """

        if not isinstance(pattern, str):
            raise TypeError("The specified pattern argument must be a string")

        if pattern not in self.__ignored_patterns__:
            self.__ignored_patterns__.append(pattern)

    @property
    def allowed_extensions(self) -> list:
        """Retrieve the allowed extensions list from collection

        Returns
        -------
        list
            The allowed extensions list
        """

        return self.__allowed_extensions__

    @property
    def ignored_patterns(self) -> list:
        """Retrieve the ignored patterns list from collection

        Returns
        -------
        list
            The ignored patterns list
        """

        return self.__ignored_patterns__

    def is_ext_allowed(self, path: Path) -> bool:
        """Check if specified file extension can be loaded

        Parameters
        ----------
        path : pathlib.Path
            File path object

        Returns
        -------
        bool
            True is specified path extension is allowed, False otherwise

        Raises
        ------
        TypeError
            When the specified path argument is not a Path object
        """

        if not isinstance(path, Path):
            raise TypeError("The specified path argument must be a Path object")

        if len(self.__allowed_extensions__) == 0:
            return True

        return path.suffix in self.__allowed_extensions__

    def is_ignored(self, path: Path) -> bool:
        """Check if specified file name can be loaded

        Parameters
        ----------
        path : pathlib.Path
            File path object

        Returns
        -------
        bool
            True is specified path name is ignored, False otherwise

        Raises
        ------
        TypeError
            When the specified path argument is not a Path object
        """

        if not isinstance(path, Path):
            raise TypeError("The specified path argument must be a Path object")

        if len(self.__ignored_patterns__) == 0:
            return False

        return any(
            [
                match(element, path.name, IGNORECASE) is not None
                for element in self.__ignored_patterns__
            ]
        )

    def remove_allowed_extension(self, extension: str):
        """Remove an extension from the allowed extensions list

        Notes
        -----
        If the specified extension does not exists in allowed extensions list,
        this method do nothing.

        Parameters
        ----------
        extension : str
            The extension to remove from collection allowed extensions list

        Raises
        ------
        TypeError
            When the specified extension argument is not a string
        """

        if not isinstance(extension, str):
            raise TypeError("The specified extension argument must be a string")

        if extension in self.__allowed_extensions__:
            self.__allowed_extensions__.remove(extension)

    def remove_ignored_pattern(self, pattern: str):
        """Remove a pattern from the ignored patterns list

        Notes
        -----
        If the specified pattern does not exists in ignored patterns list, this
        method do nothing.

        Parameters
        ----------
        pattern : str
            The pattern to remove from collection ignored patterns list

        Raises
        ------
        TypeError
            When the specified pattern argument is not a string
        """

        if not isinstance(pattern, str):
            raise TypeError("The specified pattern argument must be a string")

        if pattern in self.__ignored_patterns__:
            self.__ignored_patterns__.remove(pattern)
