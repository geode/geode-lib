# ------------------------------------------------------------------------------
#  Copyleft 2021-2022  PacMiam
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Python
from configparser import ConfigParser
from pathlib import Path

# Geode
from geode_lib.filesystem import File


class Configuration(ConfigParser, File):
    """Represents a configuration file on filesystem

    See Also
    --------
    configparser.Configparser
    geode_lib.filesystem.File
    """

    def __init__(self, *args, **kwargs):
        """Constructor"""

        File.__init__(self, *args)
        ConfigParser.__init__(self, **kwargs)

        if self.exists() and self.is_readable():
            self.read(self.path)

    def save(self, path=None, force_if_exists=False):
        """Save current configuration data on a specific file

        By default, the data will be saved in the file path specified in this
        class constructor

        Parameters
        ----------
        path : pathlib.Path or str, default: None
            File path used to save the configuration data
        force_if_exists : bool, default: False
            Force saving if the specified file does exists, otherwise an
            exception will be raised

        Raises
        ------
        FileExistsError
            When the specified path already exists and the force_if_exists
            parameter is set as False
        IsADirectoryError
            When the specified path (or inner file path) is a directory
        """

        if path is None:
            path = self.path

        elif isinstance(path, str):
            path = Path(path)

        if path.is_dir():
            raise IsADirectoryError(
                f"Cannot save: '{self.path}' is a directory"
            )

        if not force_if_exists and not self.path == path and path.exists():
            raise FileExistsError(f"Cannot save: '{path}' already exists")

        with path.open("w") as fileobject:
            self.write(fileobject, space_around_delimiters=True)
